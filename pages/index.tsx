import React, { Suspense } from 'react';
import Loader from '../src/components/loader';

const Home = React.lazy(() => import('../src/components/home'));

export default function IndexPage() {
  return <Suspense fallback={<Loader />}><Home /></Suspense>;
}
