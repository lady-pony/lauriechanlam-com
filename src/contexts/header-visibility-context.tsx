import React from 'react';

const HeaderVisibilityContext = React.createContext<boolean | undefined>(undefined);

export default HeaderVisibilityContext;
