import {
  Box, BoxTypes, Button, TabExtendedProps, TabsExtendedProps,
} from 'grommet';
import {
  DirectionType, HeightType, MarginType, PadType, WidthType,
} from 'grommet/utils';
import { get } from 'lodash-es';
import React, { useState } from 'react';

type DirectionProps = {
  direction: DirectionType;
  nav: BoxTypes;
  tab: {
    margin?: MarginType;
    pad?: PadType;
  };
};

const COLUMN_PROPS: DirectionProps = {
  direction: 'row',
  nav: {
    border: { side: 'right', size: 'medium' },
    direction: 'column',
    gap: 'none',
    pad: 'none',
  },
  tab: {
    margin: { right: '-4px' },
  },
};

const ROW_PROPS: DirectionProps = {
  direction: 'column',
  nav: {
    border: { side: 'bottom', size: 'medium' },
    direction: 'row',
    gap: 'none',
    justify: 'center',
    margin: { vertical: 'medium' },
  },
  tab: {
    margin: { bottom: '-4px' },
  },
};

type TabHeaderProps = {
  a11yTitle?: string,
  active?: boolean;
  onClick?: () => void;
  title: React.ReactNode;
};

function TabHeader({
  a11yTitle,
  active,
  onClick,
  title,
}: TabHeaderProps) {
  return (
    <Button
      a11yTitle={a11yTitle}
      active={active}
      onClick={onClick}
      plain
    >
      <Box
        background={active ? 'brand' : undefined}
        fill
        pad={{ horizontal: 'medium', vertical: 'xsmall' }}
      >
        {title}
      </Box>
    </Button>
  );
}

TabHeader.defaultProps = {
  a11yTitle: undefined,
  active: false,
  onClick: undefined,
};

type TabHeadersProps = {
  activeIndex?: number;
  children?: React.ReactNode;
  directionProps: DirectionProps;
  height?: HeightType;
  onActiveIndexChanged?: (_: number) => void;
  width?: WidthType;
};

function TabHeaders({
  activeIndex,
  children,
  directionProps,
  height,
  onActiveIndexChanged,
  width,
}: TabHeadersProps) {
  return (
    <Box {...directionProps.nav} height={height} width={width}>
      {React.Children.map(children, (element, index) => {
        if (!React.isValidElement(element)) {
          return null;
        }
        const { a11yTitle, title } = element.props as TabExtendedProps;
        return (
          <TabHeader
            {...directionProps.tab}
            a11yTitle={a11yTitle}
            active={activeIndex === index}
            onClick={() => onActiveIndexChanged?.(index)}
            title={title}
          />
        );
      })}
    </Box>
  );
}

TabHeaders.defaultProps = {
  activeIndex: undefined,
  children: undefined,
  height: undefined,
  onActiveIndexChanged: undefined,
  width: undefined,
};

function Tabs({
  activeIndex: _activeIndex,
  children,
  direction,
  height,
  width,
}: TabsExtendedProps & {
  direction: DirectionType;
  height?: HeightType;
  width?: WidthType;
}) {
  const props = direction === 'column' ? COLUMN_PROPS : ROW_PROPS;
  const [activeIndex, setActiveIndex] = useState(_activeIndex || 0);
  return (
    <Box direction={props.direction} justify="start" fill>
      <TabHeaders
        activeIndex={activeIndex}
        onActiveIndexChanged={setActiveIndex}
        directionProps={props}
        height={height}
        width={width}
      >
        {children}
      </TabHeaders>
      {React.Children.map(children, (element, index) => {
        if (index !== activeIndex || !React.isValidElement(element)) {
          return null;
        }
        return get(element.props, 'children', null) as (React.ReactNode | null);
      })}
    </Box>
  );
}

Tabs.defaultProps = {
  height: undefined,
  width: undefined,
};

export default Tabs;
