import {
  Book, Code, Language, Music, Run, StatusUnknown, User,
} from 'grommet-icons';

import { Avatar, AvatarExtendedProps } from 'grommet';
import { isUndefined } from 'lodash-es';
import React from 'react';

const Icons = [
  Book,
  Code,
  Language,
  Music,
  Run,
  User,
];

const SIZES = [
  'xsmall',
  'small',
  'medium',
  'large',
  'xlarge',
  '2xl',
  '3xl',
  '4xl',
  '5xl',
];

function AvatarIcon({
  color, icon, size, ...props
}: AvatarExtendedProps & { icon: string }) {
  const Icon = Icons.find(({ displayName }) => displayName === icon) || StatusUnknown;
  const sizeIndex = isUndefined(size) ? -1 : SIZES.indexOf(size);
  const iconSize = sizeIndex < 1 ? size : SIZES[sizeIndex - 1];
  return (
    <Avatar {...props} size={size}>
      <Icon color={color} size={iconSize} />
    </Avatar>
  );
}

export default AvatarIcon;
