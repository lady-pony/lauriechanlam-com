import {
  Avatar, Box, Button, ButtonExtendedProps, DropButton, Heading, Menu, ResponsiveContext,
} from 'grommet';
import React, { useEffect, useState } from 'react';
import { Menu as MenuIcon } from 'grommet-icons';
import { ColorType } from 'grommet/utils';
import { scroller } from 'react-scroll';
import AnimatedHeader from './animated-header';
import HeaderVisibilityContext from '../contexts/header-visibility-context';
import i18n from '../i18n/i18n';

const LANGUAGES = [
  { emoji: '🇬🇧', label: 'English', lang: 'en' },
  { emoji: '🇫🇷', label: 'Français', lang: 'fr' },
];

type MenuBarItem = {
  a11yTitle: string;
  id: string;
  label: React.ReactNode;
};

type SectionLinkProps = {
  containerId: string | undefined;
  item: MenuBarItem;
  onClick?: () => void;
};

type MenuItemButtonProps = {
  a11yTitle?: string;
  active?: boolean;
  label: React.ReactNode;
  onClick?: () => void;
};

function PaddedButton({ children, ...props }: ButtonExtendedProps & { children: React.ReactNode }) {
  return (
    <Button {...props}>
      <Box pad={{ horizontal: 'small', vertical: 'xsmall' }}>{children}</Box>
    </Button>
  );
}

function Resume() {
  const lang = i18n.language.slice(0, 2);
  return (
    <PaddedButton
      a11yTitle="CV"
      href={`../CV-laurie-chan-lam-${lang}.pdf`}
      target="_blank"
    >
      CV
    </PaddedButton>
  );
}

function LargeLanguageSelector({ background }: { background?: string }) {
  const lang = i18n.language.slice(0, 2);
  return (
    <Menu
      dropBackground={background}
      label={LANGUAGES.find((language) => language.lang === lang)?.emoji}
      items={LANGUAGES.map((language) => (
        {
          active: language.lang === lang,
          label: `${language.emoji} ${language.label}`,
          onClick: () => i18n.changeLanguage(language.lang),
        }
      ))}
    />
  );
}

LargeLanguageSelector.defaultProps = {
  background: undefined,
};

function MenuItemButton({
  a11yTitle, active, label, onClick,
}: MenuItemButtonProps) {
  const [isHeaderVisible, setHeaderVisible] = useState(true);
  return (
    <HeaderVisibilityContext.Provider value={isHeaderVisible}>
      <PaddedButton
        a11yTitle={a11yTitle}
        active={active}
        onClick={() => {
          setHeaderVisible(false);
          onClick?.();
        }}
      >
        {label}
      </PaddedButton>
    </HeaderVisibilityContext.Provider>
  );
}

MenuItemButton.defaultProps = {
  a11yTitle: undefined,
  active: undefined,
  onClick: undefined,
};

function SectionLink({
  containerId,
  item,
  onClick,
}: SectionLinkProps) {
  return (
    <MenuItemButton
      a11yTitle={item.a11yTitle}
      label={item.label}
      onClick={() => {
        scroller.scrollTo(item.id, { containerId, smooth: true });
        onClick?.();
      }}
    />
  );
}

SectionLink.defaultProps = {
  onClick: undefined,
};

type SmallMenuBarProps = {
  background?: ColorType;
  containerRef: React.RefObject<HTMLElement>;
  items: MenuBarItem[];
};

function SmallMenuBar({
  background, containerRef, items,
}: SmallMenuBarProps) {
  const [open, setOpen] = useState(false);
  const container = containerRef.current;
  const containerId = container?.id;

  useEffect(() => {
    const scrollHandler = () => setOpen(false);
    container?.addEventListener('scroll', scrollHandler);
    return () => {
      container?.removeEventListener('scroll', scrollHandler);
    };
  }, [container, setOpen]);

  return (
    <Box align="center" direction="row" gap="none" justify="center">
      <Resume />
      <DropButton
        dropContent={(
          <Box background={background} pad="small">
            {items.map((item) => (
              <SectionLink
                containerId={containerId}
                item={item}
                key={item.id}
                onClick={() => setOpen(false)}
              />
            ))}
            {LANGUAGES.map((language) => (
              <MenuItemButton
                key={language.lang}
                a11yTitle={language.label}
                active={language.lang === i18n.language.slice(0, 2)}
                label={`${language.emoji} ${language.label}`}
                onClick={() => {
                  setOpen(false);
                  i18n
                    .changeLanguage(language.lang)
                    .catch(console.warn);
                }}
              />
            ))}
          </Box>
      )}
        dropProps={{ align: { top: 'bottom', right: 'right' } }}
        label={<Box><MenuIcon /></Box>}
        margin="auto"
        open={open}
        onOpen={() => setOpen(true)}
        onClose={() => setOpen(false)}
      />
    </Box>
  );
}

SmallMenuBar.defaultProps = {
  background: undefined,
};

type LargeMenuBarProps = {
  background?: string;
  containerId?: string;
  items: MenuBarItem[];
};

function LargeMenuBar({
  background,
  containerId,
  items,
}: LargeMenuBarProps) {
  return (
    <Box align="center" direction="row" gap="none" justify="center">
      {items.map((item) => (
        <SectionLink
          containerId={containerId}
          item={item}
          key={item.id}
        />
      ))}
      <Resume />
      <LargeLanguageSelector background={background} />
    </Box>
  );
}

LargeMenuBar.defaultProps = {
  background: undefined,
  containerId: undefined,
};

export default function MenuBar({
  background,
  containerRef,
  items,
}: {
  background?: string;
  containerRef: React.RefObject<HTMLElement>;
  items: MenuBarItem[];
}): JSX.Element {
  return (
    <AnimatedHeader
      background={background}
      containerRef={containerRef}
      gap="none"
      pad="xsmall"
    >
      <SectionLink
        containerId={containerRef.current?.id}
        item={{
          a11yTitle: 'Laurie Chan Lam Presentation',
          id: 'presentation',
          label: (
            <Box direction="row" align="center" gap="medium">
              <Avatar flex={false} responsive src="images/avatar.jpg" />
              <Heading level={4} margin="none">Laurie Chan Lam</Heading>
            </Box>),
        }}
      />
      <ResponsiveContext.Consumer>
        {(responsive) => (responsive === 'large' ? (
          <LargeMenuBar background={background} containerId={containerRef.current?.id} items={items} />
        ) : (
          <SmallMenuBar
            background={background}
            containerRef={containerRef}
            items={items}
          />
        ))}
      </ResponsiveContext.Consumer>
    </AnimatedHeader>
  );
}

MenuBar.defaultProps = {
  background: undefined,
};
