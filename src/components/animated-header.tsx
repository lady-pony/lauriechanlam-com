import { BoxTypes, Header } from 'grommet';

import React, { useEffect, useState } from 'react';
import { scrollSpy } from 'react-scroll';
import HeaderVisibilityContext from '../contexts/header-visibility-context';

type AnimatedHeaderProps = {
  containerRef: React.RefObject<HTMLElement>;
} & BoxTypes;

function makeStyle(visible?: boolean): React.CSSProperties {
  switch (visible) {
    case true: return { transform: 'translateY(0)' };
    case false: return { transform: 'translateY(-100%)' };
    default: return {};
  }
}

function AnimatedHeader({
  containerRef,
  style,
  ...props
}: AnimatedHeaderProps) {
  const [, setScrollY] = useState({ position: 0, delta: 0 });
  const [isHeaderVisible, setHeaderVisible] = useState(true);

  useEffect(() => {
    scrollSpy.mount(containerRef.current);
    const spyHandler = () => setScrollY((previous) => {
      const position = scrollSpy.currentPositionY(containerRef.current);
      const delta = position - previous.position;
      if (delta !== 0) {
        setHeaderVisible?.(delta < 0);
      }
      return { position, delta };
    });
    scrollSpy.addSpyHandler(spyHandler, containerRef.current);
    return () => {
      scrollSpy.unmount(null, spyHandler);
    };
  }, [containerRef]);

  return (
    <HeaderVisibilityContext.Provider value={isHeaderVisible}>
      <Header
        style={{
          position: 'fixed',
          top: 0,
          left: 0,
          right: 0,
          zIndex: 1,
          transition: 'all 0.5s ease-in-out',
          ...makeStyle(isHeaderVisible),
          ...style,
        }}
        {...props}
      />
    </HeaderVisibilityContext.Provider>
  );
}

export default AnimatedHeader;
