import React from 'react';

import i18next from 'i18next';
import {
  Box,
  Heading,
  ResponsiveContext,
  Tab,
} from 'grommet';
import { DateTime } from 'luxon';
import { DirectionType, HeightType, WidthType } from 'grommet/utils';
import { get } from 'lodash-es';
import { useTranslation } from 'react-i18next';

import Anchor from './anchor';
import Markdown from './markdown';
import Section from './section';
import Tabs from './tabs';
import config from '../assets/content/content.json';

const SECTION_ID = 'jobs';

type Job = {
  id: string,
  start: string,
  end: string,
  company: { name: string, link?: string }
};

function format(date: string, lang: string) {
  return DateTime.fromISO(date).setLocale(lang).toFormat('LLL yyyy');
}

function JobDisplay({ accentColor, job }: { accentColor?: string; job: Job }) {
  const { t: translation } = useTranslation();
  const title = translation([SECTION_ID, 'content', 'jobs', job.id, 'title'].join('.'));
  const location = translation([SECTION_ID, 'content', 'jobs', job.id, 'location'].join('.'));
  const summary = translation([SECTION_ID, 'content', 'jobs', job.id, 'summary'].join('.'));
  return (
    <Box gap="small" pad="small">
      <Heading level={3} margin="none" responsive>
        {`${title} - `}
        <Anchor color={accentColor} href={job.company.link} label={job.company.name} />
      </Heading>
      <Heading level={4} margin="none" responsive>
        {`${location} ${format(job.start, i18next.language)} - ${format(job.end, i18next.language)}`}
      </Heading>
      <Markdown accentColor={accentColor} content={summary} />
    </Box>
  );
}

JobDisplay.defaultProps = {
  accentColor: undefined,
};

function JobsSection({ accentColor, background }: { accentColor?: string; background?: string }) {
  const jobs = get(config, `${SECTION_ID}.content`) as Job[];
  return (
    <Section accentColor={accentColor} background={background} id={SECTION_ID}>
      <ResponsiveContext.Consumer>
        {(responsive) => {
          const { direction, height, width }: {
            direction: DirectionType;
            height?: HeightType;
            width?: WidthType;
          } = responsive === 'large'
            ? { direction: 'column', height: '18em', width: 'small' }
            : { direction: 'row', height: undefined, width: undefined };
          return (
            <Tabs direction={direction} height={height} width={width}>
              {jobs.map((job) => (
                <Tab key={job.id} a11yTitle={job.company.name} color="brand" title={job.company.name}>
                  <JobDisplay accentColor={accentColor} job={job} />
                </Tab>
              ))}
            </Tabs>
          );
        }}
      </ResponsiveContext.Consumer>
    </Section>
  );
}

JobsSection.defaultProps = {
  accentColor: undefined,
  background: undefined,
};

export default JobsSection;
