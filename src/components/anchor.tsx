import { AnchorExtendedProps, Anchor as GrommetAnchor } from 'grommet';

import React from 'react';

function Anchor({ children, href, ...props }: Omit<AnchorExtendedProps, 'rel' | 'target'>) {
  return <GrommetAnchor href={href} target="_blank" rel="noopener" {...props}>{children}</GrommetAnchor>;
}

export default Anchor;
