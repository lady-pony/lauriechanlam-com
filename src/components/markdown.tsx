import React from 'react';
import {
  Anchor,
  AnchorExtendedProps,
  Box,
  Markdown as GrommetMarkdown,
  Text,
} from 'grommet';
import { FormCheckmark, FormNext } from 'grommet-icons';
import { flattenDeep, isString } from 'lodash-es';

type MarkdownItemProps = {
  accentColor?: string;
  children?: React.ReactNode;
};

function MarkdownItem({ accentColor, children }: MarkdownItemProps) {
  // eslint-disable-next-line react/no-unstable-nested-components
  function Link(props: AnchorExtendedProps) {
    return <Anchor {...props} color={accentColor} style={{ fontWeight: 'bold' }} />;
  }
  // eslint-disable-next-line react/no-unstable-nested-components
  function Strong({ children: strongChildren }: { children: React.ReactNode; }) {
    return <Text color={accentColor} weight="bold">{strongChildren}</Text>;
  }
  return (
    <GrommetMarkdown
      options={{ overrides: { a: Link, strong: Strong } }}
    >
      {children}
    </GrommetMarkdown>
  );
}

MarkdownItem.defaultProps = {
  accentColor: undefined,
  children: undefined,
};

type Hierarchy = (string | Hierarchy)[];

type BulletType = '>' | '=>' | null;

function partitionBullet(text: string): { bullet: BulletType; text: string } {
  if (text.startsWith('> ')) {
    return { bullet: '>', text: text.slice(2) };
  }
  if (text.startsWith('=> ')) {
    return { bullet: '=>', text: text.slice(3) };
  }
  return { bullet: null, text };
}

function makeLines(children: MarkdownContent, indentationLevel = 0): {
  content: { bullet: BulletType; text: string };
  indentationLevel: number
}[] {
  if (isString(children)) {
    return [{ content: partitionBullet(children), indentationLevel }];
  }
  return flattenDeep(children.map((child) => makeLines(child, indentationLevel + 1)));
}

function Bullet({ bullet, color }: { bullet: BulletType;color?: string }) {
  switch (bullet) {
    case '>':
      return <FormNext color={color} />;
    case '=>':
      return <FormCheckmark color={color} />;
    case null:
      return null;
    default: throw new Error('Unknown BulletType');
  }
}

Bullet.defaultProps = {
  color: undefined,
};

type MarkdownProps = {
  accentColor?: string;
  align?: string;
  content: MarkdownContent;
};

function Markdown({
  accentColor, align, content,
}: MarkdownProps) {
  const lines = makeLines(content, isString(content) ? 0 : -1);
  return (
    <Box align={align} gap="xsmall" pad="xsmall">
      {lines.map(({ content: { bullet, text }, indentationLevel }) => (
        <Box key={text} direction="row" align="top" margin={{ left: `${indentationLevel * 30}px` }}>
          <Bullet bullet={bullet} color={accentColor} />
          <MarkdownItem accentColor={accentColor}>
            {text}
          </MarkdownItem>
        </Box>
      ))}
    </Box>
  );
}

Markdown.defaultProps = {
  accentColor: undefined,
  align: undefined,
};

export default Markdown;
export type MarkdownContent = string | Hierarchy;
