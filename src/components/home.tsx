import React from 'react';
import Layout from './layout';
import AboutMeSection from './about-me-section';
import ContactSection from './contact-section';
import EducationSection from './education-section';
import HobbiesSection from './hobbies-section';
import JobsSection from './jobs-section';
import PresentationSection from './presentation-section';
import SkillsSection from './skills-section';

function Home() {
  return (
    <Layout>
      <PresentationSection />
      <AboutMeSection accentColor="accent-3" background="accent-1" />
      <SkillsSection accentColor="accent-3" background="accent-2" />
      <JobsSection accentColor="accent-3" background="accent-1" />
      <EducationSection accentColor="accent-3" background="accent-2" />
      <HobbiesSection accentColor="accent-3" background="accent-1" />
      <ContactSection color="accent-1" background="brand" />
    </Layout>
  );
}

export default Home;
