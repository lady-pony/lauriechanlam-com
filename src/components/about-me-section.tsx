import React from 'react';

import { Avatar, Box } from 'grommet';
import { BackgroundType } from 'grommet/utils';
import { useTranslation } from 'react-i18next';

import Markdown from './markdown';
import Section from './section';

const SECTION_ID = 'about-me';

function AboutMeSection({ accentColor, background }: { accentColor?: string; background?: BackgroundType }) {
  const { t: translation } = useTranslation();
  return (
    <Section
      accentColor={accentColor}
      background={background}
      id={SECTION_ID}
    >
      <Box align="center" gap="medium">
        <Avatar size="2xl" src="images/avatar.jpg" />
        <Markdown
          accentColor={accentColor}
          align="center"
          content={translation([SECTION_ID, 'content', 'text'].join('.'))}
        />
      </Box>
    </Section>
  );
}

AboutMeSection.defaultProps = {
  accentColor: undefined,
  background: undefined,
};

export default AboutMeSection;
