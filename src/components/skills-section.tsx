import React from 'react';

import {
  Box,
  BoxExtendedProps,
  Heading,
  ResponsiveContext,
  Text,
} from 'grommet';
import { ColorType } from 'grommet/utils';
import { StopFill as Square } from 'grommet-icons';
import { get, range } from 'lodash-es';
import { useTranslation } from 'react-i18next';

import AvatarIcon from './avatar-icon';
import Section from './section';
import config from '../assets/content/content.json';

const SECTION_ID = 'skills';

type Skill = { id: string; emoji?: string; value: number };
type Skillset = { id: string; icon: string; skills: Skill[] };

const LARGE_PROPS: BoxExtendedProps = {
  direction: 'row',
  fill: true,
  gap: 'large',
  justify: 'evenly',
};

const SMALL_PROPS: BoxExtendedProps = {
  direction: 'column',
  gap: 'xlarge',
};

function SkillDisplay({
  skillsetId, id, emoji, value,
}: { skillsetId: string } & Skill) {
  const { t: translation } = useTranslation();
  const name = translation([SECTION_ID, 'content', 'skillsets', skillsetId, 'skills', id].join('.'));
  const maxLevel = 5;
  const level = Math.floor(Math.max(Math.min(value, 1), 0) * maxLevel);
  return (
    <Box direction="row" gap="medium" justify="between" width="100%">
      <Text>
        {emoji && `${emoji} `}
        {name}
      </Text>
      <Box direction="row">
        {range(0, level).map((index) => <Square key={index} color="brand" />)}
      </Box>
    </Box>
  );
}

SkillDisplay.defaultProps = {
  emoji: undefined,
};

function SkillsetDisplay({ background, skillset }: { background?: string; skillset: Skillset }) {
  const { t: translation } = useTranslation();
  const name = translation([SECTION_ID, 'content', 'skillsets', skillset.id, 'name'].join('.'));
  return (
    <Box align="center" direction="column" gap="medium">
      <Box direction="row" gap="medium">
        <AvatarIcon
          a11yTitle={name}
          background="brand"
          color={background}
          flex={false}
          icon={skillset.icon}
          size="large"
        />
        <Heading level={3} responsive>{name}</Heading>
      </Box>
      <Box direction="column" fill gap="small">
        {skillset.skills.map(({ id, emoji, value }) => (
          <SkillDisplay key={id} skillsetId={skillset.id} id={id} emoji={emoji} value={value} />
        ))}
      </Box>
    </Box>
  );
}

SkillsetDisplay.defaultProps = {
  background: undefined,
};

type SkillsSectionProps = {
  accentColor?: ColorType;
  background?: string;
};

function SkillsSection({
  accentColor,
  background,
}: SkillsSectionProps) {
  const skillsets = get(config, `${SECTION_ID}.content`) as Skillset[];
  return (
    <Section accentColor={accentColor} background={background} id={SECTION_ID}>
      <ResponsiveContext.Consumer>
        {(responsive) => (
          <Box {...(responsive === 'large' ? LARGE_PROPS : SMALL_PROPS)}>
            {skillsets.map((skillset) => (
              <SkillsetDisplay
                key={skillset.id}
                background={background}
                skillset={skillset}
              />
            ))}
          </Box>
        )}
      </ResponsiveContext.Consumer>
    </Section>
  );
}

SkillsSection.defaultProps = {
  accentColor: undefined,
  background: undefined,
};

export default SkillsSection;
